/* Copyright 2012 Tobias Marschall
 * 
 * This file is part of CLEVER.
 * 
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>

#include "PackedAlignmentPair.h"

using namespace std;

PackedAlignmentPair::PackedAlignmentPair(const AlignmentPair& ap) {
	assert(ap.getEnd1()+1<=ap.getStart2()-1);
	assert(ap.getChrom1() == ap.getChrom2());
	this->name = ap.getName();
	this->chromosome = ap.getChrom1();
	this->read_group = ap.getReadGroup();
	this->insert_start = ap.getEnd1() + 1;
	this->insert_length = ap.getStart2() - insert_start;
	this->pair_nr = ap.getPairNr();
	this->aln_pair_prob_ins_length = ap.getProbabilityInsertLength();
}

PackedAlignmentPair::~PackedAlignmentPair() {
	// cout << "Destroying alignment " << id << endl;
}


size_t PackedAlignmentPair::intersectionLength(const PackedAlignmentPair & ap) const {
	int left = max(insert_start, ap.insert_start);
	int right = min(insert_start+insert_length, ap.insert_start+ap.insert_length);
	return max(0, right-left);
}
