/* Copyright 2013 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef GENOTYPEDISTRIBUTION_H
#define GENOTYPEDISTRIBUTION_H

#include <iostream>
#include <vector>
#include <memory>

class GenotypeDistribution {
private:
	std::vector<double> distribution;
public:
	typedef enum { ABSENT = 0, HETEROZYGOUS = 1, HOMOZYGOUS = 2 } genotype_t;
	
	GenotypeDistribution() : distribution(3, 1.0/3.0) {}
	GenotypeDistribution(double no_prob, double hetero_prob, double homo_prob);

	double notPresent() const { return distribution[0]; }
	double heterozygous() const { return distribution[1]; }
	double homozyguous() const { return distribution[2]; }
	double present() const { return distribution[1] + distribution[2]; }

	double& notPresent() { return distribution[0]; }
	double& heterozygous() { return distribution[1]; }
	double& homozyguous() { return distribution[2]; }

	double probability(const genotype_t& g) const { return distribution[g]; }
	double& probability(const genotype_t& g) { return distribution[g]; } 

	/** Normalize distribution such that it sums to one. */
	void normalize();

	genotype_t likeliestGenotype();

	std::string likeliestGenotypeString();

	/** Returns the probability that the likeliest genotype is wrong. */
	double errorProbability();

	/** Returns the probability that the given genotype is wrong. */
	double errorProbability(genotype_t genotype);

	/** Returns a string representation that corresponds to the PL field in VCF files. */
	std::string toPLString();

	static std::string genotypeToString(genotype_t genotype);

	friend GenotypeDistribution operator*(const GenotypeDistribution& d1, const GenotypeDistribution& d2);
};

std::ostream& operator<<(std::ostream& os, const GenotypeDistribution& d);

std::ostream& operator<<(std::ostream& os, const GenotypeDistribution::genotype_t& g);

#endif // GENOTYPEDISTRIBUTION_H
