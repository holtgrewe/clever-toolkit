/* Copyright 2012 Tobias Marschall
 * 
 * This file is part of CLEVER.
 * 
 * CLEVER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLEVER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>

#include "AlignmentPair.h"

using namespace std;

AlignmentPair::AlignmentPair(const string& line, ReadGroups* read_groups) {
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer_t;
	boost::char_separator<char> separator(" \t");
	tokenizer_t tokenizer(line,separator);
	vector<string> tokens(tokenizer.begin(), tokenizer.end());
	if (tokens.size()!=15) {
		throw std::runtime_error("Error parsing alignment pair.");
	}
	try {
		this->name = tokens[0];
		this->pair_nr = boost::lexical_cast<int>(tokens[1]);
		if (read_groups == 0) {
			this->read_group = -1;
		} else {
			this->read_group = read_groups->getIndex(tokens[2]);
			if (this->read_group == -1) {
				ostringstream oss;
				oss << "Unknown read group \"" << tokens[2] << "\" encountered in read \"" << this->name << "\"";
				throw std::runtime_error(oss.str());
			}
		}
		this->phred_sum1 = boost::lexical_cast<int>(tokens[3]);
		this->chrom1 = tokens[4];
		this->start1 = boost::lexical_cast<int>(tokens[5]);
		this->end1 = boost::lexical_cast<int>(tokens[6]);
		this->strand1 = tokens[7];
		this->phred_sum2 = boost::lexical_cast<int>(tokens[8]);
		this->chrom2 = tokens[9];
		this->start2 = boost::lexical_cast<int>(tokens[10]);
		this->end2 = boost::lexical_cast<int>(tokens[11]);
		this->strand2 = tokens[12];
		this->aln_pair_prob = boost::lexical_cast<double>(tokens[13]);
		this->aln_pair_prob_ins_length = boost::lexical_cast<double>(tokens[14]);
	} catch(boost::bad_lexical_cast &){
		throw std::runtime_error("Error parsing alignment pair.");
	}
}
