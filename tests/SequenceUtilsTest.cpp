#include <boost/test/unit_test.hpp>
#include "../src/SequenceUtils.h"

using namespace std;

BOOST_AUTO_TEST_CASE( SequenceUtilsTest ) {
	string ref    = "TGTCTGAATTGCAGCACTGCGCGTCTATGAGTTGGTGTCGGCAAAGCTGACAGAGATATTGGCGCAACCCCCTACTGCTGTTGCAAAATTGAGGCAAATA";
	string query6 = "ATGAGTCTCACTGACGCGGC";
	string query1 = "GCATCACTGCCGTCTTGCCGTCATGAGTCGTTAAATCGCCGGTAGACCAGGACTAGGC";
	string query2 = "AGTCTGCAATTTCATTGCCGTCTTGCCGTCATGAGTCGTTAAATCGCCGGTAGACCAGGACTAGGC";
	string query3 = "GCAGCACTGC";
	string query4 = "ACATACCCGCT";
	string query5 = "CTGTCTGTTGC";
	SequenceUtils::extension_data_t ed =  SequenceUtils::bandedExtension(ref, query1, 10, 2, 0.2);
	BOOST_CHECK_EQUAL(ed.errors, 3);
	BOOST_CHECK_EQUAL(ed.query_length, 17);
	BOOST_CHECK_EQUAL(ed.ref_end_pos, 28);
	ed =  SequenceUtils::bandedExtension(ref, query2, 0, 1, 0.1);
	BOOST_CHECK_EQUAL(ed.errors, 1);
	BOOST_CHECK_EQUAL(ed.query_length, 6);
	BOOST_CHECK_EQUAL(ed.ref_end_pos, 5);
	ed =  SequenceUtils::bandedExtension(ref, query2, 0, 1, 2.0/7.0);
	BOOST_CHECK_EQUAL(ed.errors, 3);
	BOOST_CHECK_EQUAL(ed.query_length, 14);
	BOOST_CHECK_EQUAL(ed.ref_end_pos, 12);
	ed =  SequenceUtils::bandedExtension(ref, query3, 10, 0, 0.0);
	BOOST_CHECK_EQUAL(ed.errors, 0);
	BOOST_CHECK_EQUAL(ed.query_length, 10);
	BOOST_CHECK_EQUAL(ed.ref_end_pos, 19);
	ed =  SequenceUtils::bandedExtension(ref, query4, 95, 1, 0.1);
	BOOST_CHECK_EQUAL(ed.errors, 1);
	BOOST_CHECK_EQUAL(ed.query_length, 5);
	BOOST_CHECK_EQUAL(ed.ref_end_pos, 99);
	ed =  SequenceUtils::bandedExtension(ref, query3, 19, 0, 0.0, true);
	BOOST_CHECK_EQUAL(ed.errors, 0);
	BOOST_CHECK_EQUAL(ed.query_length, 10);
	BOOST_CHECK_EQUAL(ed.ref_end_pos, 10);
	ed =  SequenceUtils::bandedExtension(ref, query5, 11, 2, 0.0, true);
	BOOST_CHECK_EQUAL(ed.errors, 2);
	BOOST_CHECK_EQUAL(ed.query_length, 10);
	BOOST_CHECK_EQUAL(ed.ref_end_pos, 0);
	ed =  SequenceUtils::bandedExtension(ref, query6, 24, 2, 0.2, true);
	BOOST_CHECK_EQUAL(ed.errors, 2);
	BOOST_CHECK_EQUAL(ed.query_length, 12);
	BOOST_CHECK_EQUAL(ed.ref_end_pos, 14);
}
