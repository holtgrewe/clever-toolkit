#include <boost/test/unit_test.hpp>
#include "../src/ShortDnaSequence.h"

#include <sstream>

using namespace std;

BOOST_AUTO_TEST_CASE( ShortDnaSequenceTest ) {
	ShortDnaSequence s("aacCCgNnnnNCAGMTAG", "E6DB@#?GGD3DB;DE#D");
	ostringstream oss;
	oss << s;
	BOOST_CHECK_EQUAL(oss.str(), "AACCCGNNNNNCAGNTAG");
	ostringstream oss2;
	oss2 << s.reverseComplement();
	BOOST_CHECK_EQUAL(oss2.str(), "CTANCTGNNNNNCGGGTT");
}
